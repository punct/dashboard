import xs from 'xstream'
import { div } from '@cycle/dom'
import { prettyTime, getService } from '../lib/utils'

function intent (sources) {
  return {
    musicData$: getService(sources.WS, 'music')
  }
}

function model (actions) {
  const music$ = actions.musicData$
    .map(({ artist, track, length, position }) => ({
      artist,
      track,
      length,
      position
    }))
  return {
    music$
  }
}

function view (models) {
  const vtree$ = models.music$
    .map(music => ({
      artist: music.artist,
      track: music.track,
      position: prettyTime(music.position),
      length: prettyTime(music.length),
      progress: `${music.position / music.length * 100}%`
    }))
    .map(music =>
      div('.music-panel', [
        div('.music-track', `${music.track}`),
        div('.music-length', [
          div('.music-progress', { style: { width: music.progress } }),
          div('.music-tracker', `${music.position} / ${music.length}`)
        ]),
        div('.music-artist', `${music.artist}`)
      ])
  )
  return vtree$
}

function main (sources) {
  const actions = intent(sources)
  const models = model(actions)
  const vtree$ = view(models)
  return {
    DOM: vtree$
  }
}

export default main
