import xs from 'xstream'
import { div, span } from '@cycle/dom'
import { getService } from '../lib/utils'

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

function intent (sources) {
  return {
    weatherData$: getService(sources.WS, 'weather')
      .map(data => JSON.parse(data))
      .debug('www'),
    timeData$: getService(sources.WS, 'time')
  }
}

function model (actions) {
  const time$ = actions.timeData$
    .map(timestamp => new Date(timestamp))
    .map(time => ({
      time: time / 1,
      hour: time.getHours(),
      minute: time.getMinutes(),
      second: time.getSeconds(),
      date: time.getDate(),
      month: time.getMonth(),
      weekday: time.getDay()
    }))
  const weather$ = actions.weatherData$
    .map(weather => ({
      currently: weather.currently,
      today: weather.daily.data[0],
      timezone: weather.timezone
    }))
    .debug('weather')
    .map(({ currently, today, timezone }) => ({
      current: Math.floor(currently.temperature),
      high: Math.floor(today.temperatureMax),
      low: Math.floor(today.temperatureMin),
      condition: currently.summary,
      feelslike: currently.apparentTemperature,
      humidity: currently.humidity,
      wind: {
        speed: currently.windSpeed,
        bearing: currently.windBearing,
        gust: currently.windGust
      },
      location: timezone,
      unit: 'C'
    }))
  return {
    time$,
    weather$
  }
}

function view (models) {
  const time$ = models.time$
    .map(time => ({
      hour: `${(time.hour < 10) ? '0' : ''}${time.hour}`,
      minute: `${(time.minute < 10) ? '0' : ''}${time.minute}`,
      meridian: (time.hour < 12) ? 'AM' : 'PM',
      weekday: days[time.weekday],
      day: time.date,
      month: months[time.month]
    }))
  const weather$ = models.weather$
    .map(weather => ({
      current: `${weather.current}°`,
      unit: `${weather.unit}`,
      high: `${weather.high}°`,
      low: `${weather.low}°`,
      condition: weather.condition,
      location: weather.location
    }))
  const vtree$ = xs.combine(time$, weather$)
    .map(([time, weather]) =>
      div('.main-panel', [
        div('.clock', [
          div('.time-today', 'Today'),
          div('.time-current', `${time.hour}:${time.minute}`),
          div('.time-meridian', `${time.meridian}`)
        ]),
        div('.main-panel-separator'),
        div('.info', [
          div('.time-day', `${time.weekday}, ${time.day} ${time.month}`),
          div('.weather-current', [
            div('.weather-current-temperature', [
              span('.weather-temperature', `${weather.current}`),
              span('.weather-unit', `${weather.unit}`)
            ]),
            span('.weather-current-separator'),
            span('.weather-condition', `${weather.condition}`)
          ]),
          div('.weather-extremes', `Hi ${weather.high} | Lo ${weather.low} | ${weather.location}`)
        ])
      ])
  )
  return vtree$
}

function main (sources) {
  const actions = intent(sources)
  const models = model(actions)
  const vtree$ = view(models)
  return {
    DOM: vtree$
  }
}

export default main
