import xs from 'xstream'
import { div } from '@cycle/dom'
import ClockWidget from './components/clockWidget'
import MusicWidget from './components/musicWidget'

require('../public/main.css')

function main (sources) {
  const clockWidget = ClockWidget(sources)
  const musicWidget = MusicWidget(sources)

  const vtree$ = xs.combine(
    clockWidget.DOM,
    musicWidget.DOM
  )
    .map(([clockWidgetView, musicWidgetView]) =>
      div('.homepage', [
        clockWidgetView,
        musicWidgetView
      ])
    )

  return {
    DOM: vtree$
  }
}

export default main
