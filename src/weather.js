// import xs from 'xstream'
import { div, table, thead, tbody, tr, td, span } from '@cycle/dom'
import { getService, dateFormat } from './lib/utils'

require('../public/weather.css')

function intent (sources) {
  return {
    weatherData$: getService(sources.WS, 'weather')
      .map(body => JSON.parse(body))
  }
}

function model (actions) {
  const weather$ = actions.weatherData$
  return {
    weather$
  }
}

function weatherIcon (code) {
  const icons = {
    'clear-day': '.wi.wi-day-sunny',
    'clear-night': '.wi.wi-night-clear',
    'rain': '.wi.wi-rain',
    'snow': '.wi.wi-snow',
    'sleet': '.wi.wi-sleet',
    'wind': '.wi.wi-windy',
    'fog': '.wi.wi-fog',
    'cloudy': '.wi.wi-cloudy',
    'partly-cloudy-day': '.wi.wi-day-cloudy',
    'partly-cloudy-night': '.wi.wi-night-cloudy',
    default: '.wi.wi-na'
  }
  return span(icons[code] || icons.default)
}

function current (current) {
  return div('.current', [
    div('.time', `Time: ${dateFormat(current.time, 'MMMM Do YYYY, h:mm:ss a')}`),
    div('.temperature', `Temperature: ${current.temperature}`),
    div('.feelslike', `Feels like: ${current.apparentTemperature}`),
    div('.humidity', `Humidity: ${current.humidity}%`),
    div('.skytext', [
      weatherIcon(current.icon),
      `${current.summary}`
    ]),
    div('.uvindex', `UV Index: ${current.uvIndex}`),
    div('.pressure', `Pressure: ${current.pressure} mbar`),
    div('.precipitation', `Precipitation: ${current.precipType} ${current.precipProbability}% ${current.precipIntensity} cm/h`),
    div('.cloudcover', `Cloud Cover: ${current.cloudCover}`),
    div('.windspeed', `Wind speed: ${current.windSpeed} km/h ${current.windBearing}`),
    div('.windgust', `Wind gust: ${current.windGust} km/h`)

  ])
}

function forecast (forecast) {
  return table('.forecast', [
    tr('.days',
      forecast.map((f, day) => td('.day', dateFormat(f.time, 'dddd')))
    ),
    tr('.skies',
      forecast.map((f, day) => td('.sky', [
        span('.skyimg', weatherIcon(f.icon)),
        span('.skytext', f.summary)
      ]))
    ),
    tr('.highs',
      forecast.map((f, day) => td('.high', [
        `${f.temperatureMax}° `,
        span('.fa.fa-thermometer-full'),
        span('.hightime', ` ${dateFormat(f.temperatureMaxTime, 'HH:mm')}`)
      ]))
    ),
    tr('.lows',
      forecast.map((f, day) => td('.low', [
        `${f.temperatureMin}° `,
        span('.fa.fa-thermometer-empty'),
        span('.lowtime', ` ${dateFormat(f.temperatureMinTime, 'HH:mm')}`)
      ]))
    ),
    tr('.precips',
      forecast.map((f, day) => td('.precip', [
        `${f.precipProbability}% `,
        span('.fa.fa-tint')
      ]))
    )
  ])
}

function hourly (hourly) {
  return div('.hourly', [
    div('.hourly-header', [
      span('.hourly-time'),
      span('.hourly-summary'),
      span('.hourly-temperature', [ span('.wi.wi-thermometer') ]),
      span('.hourly-precip', [ span('.wi.wi-rain') ]),
      span('.hourly-wind', [ span('.wi.wi-strong-wind') ])
    ]),
    div('.hourly-body',
      hourly.map((h, index) => div(`.hourly-entry${index % 2 ? '.hourly-entry-alt' : ''}`, [
        span('.hourly-time', `${dateFormat(h.time, 'H:mm')}`),
        span('.hourly-summary', `${h.summary}`),
        span('.hourly-temperature', [
          `${h.temperature}°`,
          span('.hourly-apparent', ` ${h.apparentTemperature}°`)
        ]),
        span('.hourly-precip', [
          `${h.precipProbability}%`,
          (h.precipProbability) ? span('.hourly-apparent', ` (${h.precipIntensity} cm/h ${h.precipType})`) : ''
        ]),
        span('.hourly-wind', [
          `${h.windSpeed} km/h`,
          span('.hourly-windGust', ` (${h.windGust} km/h)`)
        ])
      ]))
    )
  ])
}

function view (models) {
  const vtree$ = models.weather$
    .map(({ daily, currently, hourly }) => ({
      currently,
      daily: daily.data,
      hourly: hourly.data
    }))
    .map(({ daily, currently, hourly }) => ({
      currently: {
        time: currently.time * 1000,
        temperature: Math.floor(currently.temperature),
        apparentTemperature: Math.floor(currently.apparentTemperature),
        humidity: Math.floor(currently.humidity * 100),
        icon: currently.icon,
        summary: currently.summary,
        uvIndex: currently.uvIndex,
        pressure: currently.pressure,
        precipType: currently.precipType,
        precipProbability: Math.floor(currently.precipProbability * 100),
        precipIntensity: currently.precipIntensity,
        cloudCover: currently.cloudCover,
        windSpeed: currently.windSpeed,
        windBearing: currently.windBearing,
        windGust: currently.windGust
      },
      daily: daily.map(f => ({
        time: f.time * 1000,
        icon: f.icon,
        summary: f.summary,
        temperatureMax: Math.floor(f.temperatureMax),
        temperatureMaxTime: f.temperatureMaxTime * 1000,
        temperatureMin: Math.floor(f.temperatureMin),
        temperatureMinTime: f.temperatureMinTime * 1000,
        precipProbability: Math.floor(f.precipProbability * 100)
      })),
      hourly: hourly.map(h => ({
        time: h.time * 1000,
        summary: h.summary,
        temperature: Math.floor(h.temperature),
        apparentTemperature: Math.floor(h.apparentTemperature),
        windSpeed: h.windSpeed,
        windGust: h.windGust,
        precipProbability: Math.floor(h.precipProbability * 100),
        precipType: h.precipType,
        precipIntensity: h.precipIntensity
      }))
    }))
    .debug('weather')
    .map(weather => div('.weather', [
      current(weather.currently),
      hourly(weather.hourly),
      forecast(weather.daily)
    ]))
  return vtree$
}

function main (sources) {
  const actions = intent(sources)
  const models = model(actions)
  const vtree$ = view(models)
  return {
    DOM: vtree$
  }
}

export default main
