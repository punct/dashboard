import moment from 'moment'

export function getService (source, name) {
  return source
    .map(message => message.data)
    .map(data => JSON.parse(data))
    .filter(data => data.name === name)
    .map(data => data.payload)
}

export function prettyTime (seconds) {
  const units = []
  const h = Math.floor(seconds / 3600)
  if (h) {
    units.push(h)
  }
  seconds %= 3600
  const m = Math.floor(seconds / 60)
  units.push(m)
  seconds %= 60
  const s = seconds
  units.push(s)

  return units
    .map(u => (u < 10) ? `0${u}` : `${u}`)
    .join(':')
}

export function dateFormat (timestamp, format) {
  return moment(timestamp).format(format)
}
