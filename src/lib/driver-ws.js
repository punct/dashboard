import xs from 'xstream'
import { adapt } from '@cycle/run/lib/adapt'

export function makeWSDriver (url) {
  let connection = null
  return () => {
    const source = xs.create({
      start: listener => {
        connection = new WebSocket(url)
        connection.onerror = (err) => {
          listener.error(err)
        }
        connection.onmessage = (msg) => {
          listener.next(msg)
        }
      },
      stop: () => {
        connection.close()
      }
    })

    return adapt(source)
  }
}
