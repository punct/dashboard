import xs from 'xstream'
import { run } from '@cycle/run'
import { makeDOMDriver, div } from '@cycle/dom'
import { makeWSDriver } from './lib/driver-ws'
import MainPage from './main'
import WeatherPage from './weather'

function main (sources) {
  const mainPage = MainPage(sources)
  const weatherPage = WeatherPage(sources)

  const vtree$ = xs.merge(
    // mainPage.DOM
    weatherPage.DOM
  )
    .map(page => div('.app', [
      div('.background'),
      page
    ]))

  return {
    DOM: vtree$
  }
}

run(main, {
  DOM: makeDOMDriver('#container'),
  WS: makeWSDriver('ws://localhost:8080')
})
