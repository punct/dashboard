const xs = require('xstream').default

function Music (args) {
  const options = Object.assign({
    interval: 1000
  }, args)

  return xs.periodic(options.interval)
    .startWith(0)
    .map(position => ({
      artist: 'Grabbitz',
      track: 'Here with me now',
      length: 243,
      position
    }))
    .filter(current => current.position <= current.length)
}

module.exports = Music
