const xs = require('xstream').default
const weather = require('weather-js')
const DarkSky = require('dark-sky')
const forecast = new DarkSky('993818e1b1590c4c99028f35360c4c66')

const longitude = 26.1037376
const latitude = 44.3885161

function getWeather () {
  return new Promise((resolve, reject) => {
    require('fs').readFile('weather.txt', (error, result) => {
      if (error) {
        return reject(error)
      }
      return resolve(result.toString())
    })
  })
  // return forecast
  //   .latitude(latitude)
  //   .longitude(longitude)
  //   .units('auto')
  //   .get()
  // return new Promise((resolve, reject) => {
    // weather.find({search: 'Bucharest, Romania', degreeType: 'C'}, (err, result) => {
    //   if (err) {
    //     return reject(err)
    //   }
    //   return resolve(result)
    // })
  // })
}

function Weather (args) {
  const options = Object.assign({
    interval: 600000
  }, args)

  return xs.periodic(options.interval)
    .startWith(null)
    .map(() => xs.fromPromise(getWeather()))
    .flatten()
}

module.exports = Weather
