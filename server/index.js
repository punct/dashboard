const xs = require('xstream').default
const WebSocket = require('ws')
const Logger = require('./lib/logger')
const weatherService = require('./weather')
const timeService = require('./time')
const musicService = require('./music')

const wss = new WebSocket.Server({ port: 8080, address: '0.0.0.0' })

function serialize (name) {
  return payload => JSON.stringify({
    name,
    payload
  })
}

wss.on('connection', function connection (ws) {
  const identity = `${ws._socket.remoteAddress}:${ws._socket.remotePort}`
  const logger = Logger(identity)

  const weather$ = weatherService()
    .map(serialize('weather'))
  const timestamp$ = timeService()
    .map(serialize('time'))
  const music$ = musicService()
    .map(serialize('music'))

  const listener = {
    next: data => {
      ws.send(data)
      logger.info('Sensor', data)
    },
    error: error => {
      logger.error('Sensor', error)
    },
    complete: () => {
      logger.info('Sensor', 'END')
    }
  }
  const services = xs.merge(
    weather$,
    timestamp$,
    music$
  )
  services.addListener(listener)

  ws.on('message', function incoming (message) {
    logger.info('Message', message)
  })

  ws.on('close', function close () {
    services.removeListener(listener)
    logger.info('Disconnect')
  })

  logger.info('Connect')
})
