function Logger (identity) {
  const [CRITICAL, ERROR, WARNING, INFO, DEBUG] = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']

  function log (severity, type, msg) {
    const now = new Date()
    const timestamp = `${now.toISOString()}`
    const eventType = `${type.toUpperCase()}`
    const message = (msg) ? ` - ${msg}` : ''
    // console.log(`[${timestamp}] ${severity} ${eventType} ${identity}${message}`)
  }

  return {
    critical: (type, message) => log(CRITICAL, type, message),
    error: (type, message) => log(ERROR, type, message),
    warning: (type, message) => log(WARNING, type, message),
    info: (type, message) => log(INFO, type, message),
    debug: (type, message) => log(DEBUG, type, message)
  }
}

module.exports = Logger
