const xs = require('xstream').default

function Time (args) {
  const options = Object.assign({
    interval: 5000
  }, args)

  return xs.periodic(options.interval)
    .startWith(null)
    .map(() => Date.now())
}

module.exports = Time
